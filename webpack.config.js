const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');
const webpack = require('webpack');
const glob = require('glob');
const purifyCSSPlugin = require('purifycss-webpack');

const bootstrapEntryPoints = require('./webpack.bootstrap.config.js');
const isProd = process.env.NODE_ENV === 'production';
const cssDev = ['style-loader','css-loader','sass-loader'];
const cssProd = ExtractTextPlugin.extract({
                fallback : 'style-loader',
                use : [
                'css-loader',
                'sass-loader'
                ],
                publicPath: '/dist/',
                allChunks: true,
            });
const cssConfig = isProd ? cssProd : cssDev;

const bootstrapConfig = isProd ? bootstrapEntryPoints.prod : bootstrapEntryPoints.dev;

module.exports = {
    entry : {
        detailpage : './src/js/detailpage.js',
        bootstrap : bootstrapConfig
    },

    output : {
        path: path.join(__dirname, "dist"),
        filename: 'js/[name].bundle.js'
    },

    module : {
        rules : [
            {
                test: /\.scss$/,
                use :cssConfig
            },
         
            {
                test : /\.pug$/,
                use : 
                [
                    {
                        loader : 'pug-loader',
                        options : 
                        {
                            pretty : true,
                         }
                    }
                ]
            },
            {
                test : /\.(jpe?g|png|svg|gif)$/i,
                use :
                [
                    'file-loader?name=img/[name].[ext]',
                 //   'file-loader?name=[name].[ext]&outputPath=img/&publicPath=img/',
                    'image-webpack-loader'
                ],
            },
            { 
                test: /\.(woff2?)$/, 
                use: 'url-loader?limit=10000&name=fonts/[name].[ext]' 
            },
            { 
                test: /\.(ttf|eot)$/, 
                use: 'file-loader?name=fonts/[name].[ext]'
            },
            // Bootstrap 3
            { 
                test:/bootstrap-sass[\/\\]assets[\/\\]javascripts[\/\\]/,
                use : 'imports-loader?jQuery=jquery' 
            }
     

        ]
    },
    devServer : {
        hot : false,
        contentBase :path.join(__dirname, 'dist'),
        compress : true,
        port : 8089,
        stats : "errors-only",
        open : true,
        index : 'detailpage.html',


    },
    plugins : [
        new HtmlWebpackPlugin({

            title: 'detailpage',
            filename: 'detailpage.html',
            minify : {
                collapseWhitespace: false,
                preserveLineBreaks: false                
            },
           // chunks : ["detailpage","bootstrap"],
            template: './src/detailpage.html',
        }),
        new ExtractTextPlugin({
            filename: 'css/style.css',
            disable : !isProd,
            allChunks : true
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),

        //Purify
       // new purifyCSSPlugin({
          //  paths : glob.sync(path.join(__dirname, 'src/*.html')),
        //})

    ]

}