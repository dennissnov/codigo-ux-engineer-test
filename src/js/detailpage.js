import css from '../stylesheet/detailpage.scss'
import $ from 'jquery'

console.log('detailpage');
window.onscroll = function() {myFunction()};
var navbar = document.getElementById("cme-navbar");
var sticky = navbar.offsetTop;
var $window = $(window);
var $videoWrap = $('.video-wrap');
var $video = $('.video');
var videoHeight = $video.outerHeight();
    

//SEARCH BAR
$('.search-bar .icon').on('click', function() {
    $(this).parent().toggleClass('active');
  });



//VIDEO 
$window.on('scroll',  function() {
  var windowScrollTop = $window.scrollTop();
  var videoBottom = videoHeight + $videoWrap.offset().top;
  
  if (windowScrollTop > videoBottom) {
    $videoWrap.height(videoHeight);
    $video.addClass('stick');
  } else {
    $videoWrap.height('auto');
    $video.removeClass('stick');
  }
});

//STICKY TOPBAR
function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}